<B>Ainox OS :</B>

Ainox OS is a x86-64 based LFS operating system that is built on the Linux kernel.
The source tarballs that have been used for the basic functionality of this operating system are present inside the "sources" directory.
With the help of the tarballs present inside the sources directory, one can build a decent
terminal-based OS without the GUI.
It is still a WIP for me but you can clone these files and start building your own Linux OS from scratch.

<img src="https://scontent.fbho1-1.fna.fbcdn.net/v/t1.0-9/28166806_2038746672832028_1781498233393670946_n.jpg?oh=895bc005082cb7c583ed5215d43ef202&oe=5B0A8015">

You shall follow the instructions to create your own OS from the following website :
<url>http://www.linuxfromscratch.org/lfs/view/stable/</url>

Note : 

-> The link for the MPC tarball is missing in the LFS guide's wget-list file. You may use my wget-list file from here in order for downloading the required missing tarball.

-> Also, you in case you have any queries regarding building and compilation of the packages, you shall create an issue here.

Happy building!
